﻿#include <iostream>
#include <ctime>
#pragma warning(disable:4996)
using namespace std;
int main()
{
    time_t T1 = time(0);
    struct tm T2;
    localtime_s(&T2, &T1);
    int Day = T2.tm_mday;

    const int N = 4;
    int mv[N][N] = 
    {
        {0, 1, 2, 3},
        {1, 2, 3, 4},
        {2, 3, 4, 5},
        {3, 4, 5, 6}
    };
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            cout << " " << mv[i][j];
        }
        if (i == Day % N)
        {
            cout << "   summa = ";
            cout << mv[i][0] + mv[i][1] + mv[i][2] + mv[i][3];
        }
        cout << "\n";
    }
}


